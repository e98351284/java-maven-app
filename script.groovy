def incrV() {
    sh 'mvn build-helper:parse-version versions:set \
        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
        versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "2374623/java-maven-app:" + "$version-$BUILD_NUMBER"
}

def buildJar() {
    echo "building the application..."
    sh 'mvn clean package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_NAME} ."
        sh "echo ${PASS} | docker login -u ${USER} --password-stdin"
        sh "docker push ${IMAGE_NAME}"
    }
} 

def deployApp() {
    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
    echo 'deploying the application...'
    sshagent(['ec2-server-key']) {
        sh "scp server-cmds.sh ec2-user@107.20.112.120:/home/ec2-user"
        sh "scp docker-compose.yaml ec2-user@107.20.112.120:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ec2-user@107.20.112.120 ${shellCmd}"
    }
}

def commitApp() {
    withCredentials([usernamePassword(credentialsId: 'gitlabSec', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/e98351284/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:master'
    }
}

return this
